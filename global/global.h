#ifndef GLOBAL_H
#define GLOBAL_H

#include <fstream>
#include <stdlib.h>

using namespace std;

//extern string glo_casename;

const double glo_g = 32.17;
const double glo_pi = 3.141592653589793;

// Physical constants

//extern double glo_PI;

/* COORDINATE AXIS

 z    y(into page)
 |    /
 |   /
 |  / 
 | /
 |/
 --------------------x

Cell numbering starts from z to y to x

*/

#endif
