#ifndef DEF_INFLOW_H
#define DEF_INFLOW_H

#include <math.h>

#include "../global/global.h"

class c_Inflow{

protected:

    // chang chen 2006 phd 
    double nu; // normalized vertical speed
    double v; // normalized induced velocity
    double muBar; // normalized forward speed
    double Vi; // induced flow velocity
    double V; //freestream velocity
    double alpha_d; // rotor descent angle
    double Omega; // Angular velocity
    double R; // radius of blade

};

class c_Inflow_Linear: public c_Inflow{

protected:
    double khi; // wake skew angle
    double mu; // advance ratio
    double k_x, k_y; 
    double lambda;

protected:

};

class c_Inflow_Linear_Drees: public c_Inflow_Linear{

    void calculate_k_x_k_y();

};

class c_Inflow_Linear_Payne: public c_Inflow_Linear{

    void calculate_k_x_k_y();

};

class c_Inflow_Linear_PittPeters: public c_Inflow_Linear{

    void calculate_k_x_k_y();

};


#endif
