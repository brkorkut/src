#include "def_inflow.h"

// METHODS for c_Inflow_Linear_Drees

void c_Inflow_Linear_Drees::calculate_k_x_k_y(){

    k_x = 4.0/3.0 * (1.0 - cos(khi) - 1.8*pow(mu, 2.0)) / sin(khi);
    k_y = -2.0 * mu;

}

void c_Inflow_Linear_Payne::calculate_k_x_k_y(){

    k_x = 4.0/3.0 * ( (mu / lambda) / (1.2 + mu / lambda)  );
    k_y = 0.0;

}

void c_Inflow_Linear_PittPeters::calculate_k_x_k_y(){

    k_x = (15.0 * glo_PI / 23.0) * tan(khi / 2.0);
    k_y = 0.0;

}
