#include "main.h"

int main(int argc, char *argv[]){

    cout << "-----------------------" << '\n';
    cout << "Rotorbit Inflow Solver V0.1 " << '\n';
    cout << "-----------------------" << '\n';

    ofstream outPerf;
    outPerf.open("../out_rtr/perf.dat");

    double V_min = 0.0;
    double V_max = 400.0;
    int intervals = 11;
    double thrust[intervals], torque[intervals];
    double Vx[intervals], Vy[intervals], Vz[intervals];
    double d_V = (V_max-V_min) / double(intervals-1);
    c_Trim Trim[intervals];

    for(int i=0; i<intervals; i++){
        Vx[i] = V_min + double(i) * d_V;
        Vy[i] = 0.0;
        Vz[i] = 0.0;
        Trim[i].init(Vx[i], Vy[i], Vz[i]);
        Trim[i].find();
        Trim[i].outputPerformance(thrust[i], torque[i]);
    }

    cout << "-----------------------" << '\n';
    for(int i=0; i<intervals; i++){
      cout << "Kemal " << thrust[i] << " " << torque[i] << '\n';
      outPerf << Vx[i] << " " << thrust[i] << " " << torque[i] << '\n';
    }

    outPerf.close();
    cout << "-----------------------" << '\n';

}

