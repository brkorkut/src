CC=g++
#CCC=mkoctfile --link-stand-alone -v
CCC=g++

GFLAGS=-g -Wall -Wunused-variable -Wno-deprecated -Wunused-but-set-variable
CFLAGS=-c -g -Wall -Wno-deprecated -Wunused-variable -Wunused-but-set-variable

OCT_INCLINK = -I/opt/octave-3.8.2/include -I/opt/octave-3.8.2/include/octave

LIBLINK= -pthread -g -rdynamic -fPIC -L/usr/local/lib/octave/3.8.2 -L/usr/local/lib -Wall -Wunused-variable
LIBLINK2= -loctinterp -loctave
INCLINK=

all: maestro.x Makefile
	rm -rf *.o

maestro.x: global.o pugixml.o def_trim.o main.o Makefile 
	$(CCC) $(LIBLINK) $(GFLAGS) global.o pugixml.o def_trim.o main.o -o maestro.x $(LIBLINK2)

global.o: global/global.cpp Makefile
	$(CC) $(COMPFLAGS) $(CFLAGS) global/global.cpp

pugixml.o: input/pugixml.cpp Makefile
	$(CC) $(COMPFLAGS) $(CFLAGS) input/pugixml.cpp

def_trim.o: trim/def_trim.cpp Makefile
	$(CC) $(OCT_INCLINK) $(COMPFLAGS) $(CFLAGS) trim/def_trim.cpp

main.o: main.cpp Makefile
	$(CC) $(OCT_INCLINK) $(COMPFLAGS) $(CFLAGS) main.cpp

###################################

clean:
	rm -rf *.o

veryclean:
	rm -rf *.o *.x
