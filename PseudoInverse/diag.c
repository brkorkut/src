//
//  diag.c
//  rtrbt
//
//  Created by Gurbuz Taha Ozdemir on 1/8/15.
//  Copyright (c) 2015 Rotorbit Engineering LLC. All rights reserved.
//

#include "diag.h"

static void diag(T* matrix,T* v,int size)
{
    for(int i=0;i<size;i++)
    {
        for(int32_t j=0;j<size;j++)
        {
            if(i==j)
                matrix[j*size+i]=v[i];
            else
                matrix[j*size+i]=0;
        }
    }
}