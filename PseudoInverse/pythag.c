//
//  pythag.c
//  rtrbt
//
//  Created by Gurbuz Taha Ozdemir on 1/8/15.
//  Copyright (c) 2015 Rotorbit Engineering LLC. All rights reserved.
//

//#include <stdio.h>
#include <math.h>
#include "nrutil.h"

float pythag(float a, float b)
//Computes (a2 + b2)1/2 without destructive underflow or overflow.
{
    float absa,absb;
    absa=fabs(a);
    absb=fabs(b);
    if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
    else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}