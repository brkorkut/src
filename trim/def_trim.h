#ifndef DEF_TRIM_H
#define DEF_TRIM_H

#include <iostream>
#include <math.h>
#include <octave/oct.h>
#include <octave/SparseQR.h>

#include "../global/global.h"
#include "../input/pugixml.hpp"

using namespace std;

class c_Air{

public:

    double density;

};

class c_Rotor{

public:

    void init();
    void setParameters();

    double radius; // (m)
    double omega;  // (rad/s)
    double solidity; 
    double AO;
    double twist;
    double gamma;
    double xr;
    double hr;
    double delta0;
    double delta2;

};

class c_Fuselage{

public:

    void init();

    double cdsf;

};

class c_Horizontal_Stabilizer{
  
public:

    void init();

    double sht; //area
    double aht; //lift slope
    double lht; //location

};

class c_Tail_Rotor{

public:

    void init();

    double omegtr; //tail rotor speed
    double rtr;    //tail rotor radius
    double aotr;   //rotor blade lift slope;
    double soltr;  //solidity
    double twisttr;//twist
    double ltr;    //location relative to cg;  
    double htr;    //location relative to cg;  

};

class c_Aircraft{

public:

    void init();
    void calculateAerodynamicForcesAndMoments(double *x, double *u, double *FM);
    void calculatePerformance(double *_x, double *_u, double &thrust, 
                              double &torque);

    // supporting classes
    c_Air _Air;
    c_Rotor Rotor;
    c_Fuselage Fuselage;
    c_Horizontal_Stabilizer Horizontal_Stabilizer;
    c_Tail_Rotor Tail_Rotor;

    double mass;
    double weight;
    double Ixx, Iyy, Izz, Ixz;
    double X, Y, Z;
    double L, M, N;

};

class c_Trim{

public:

    void init(double, double, double);
    void find();
    void outputPerformance(double &thrust, double &torque);

protected:

    // trim states based on ( www.aerojockey.com/papers/helicopter/node2.html#eq:phi )
    c_Aircraft Aircraft;
    
    double _V_trim[3], _psid_trim;
    // STATE VARIABLES
    double _x[12]; // u, v, w,  p, q, r, Phi, Theta, Psi, x, y, z

    double _x_dot[12]; // time derivative of u, v, w,  p, q, r, Phi, Theta, Psi, x, y, z
    double _FM[6];

    int nstates, nctrls, nfstates, itrim;

    // CONTROL VARIABLES
    
    double _u[4]; // theta_1c, theta_1s, theta_zero, theta_zero_TR in degrees

    double xscale[12];
    double delclin[4];
    double delxlin[12];
    int trimVars[12];

//    Matrix a_matrix;
    // methods
   
    void formEquationOfMotion(double *, double *, double *);
    void linearize(double *, double *, double *, Matrix &A_matrix, Matrix &B_matrix);

};

#endif
