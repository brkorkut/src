#include "def_trim.h"

// METHODS for c_Trim
//
//

void c_Rotor::init(){

    string s_input_folder = "../in_rtr/modelConfig.xml";
    const char* input_folder = s_input_folder.c_str();

    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(input_folder);

    radius = atof(doc.child("main").child("model").child("rotor").
           child_value("radius"));
    omega = 2.0 * glo_pi / 60.0 * (atof(doc.child("main").child("model").
           child("rotor").child_value("omega")));
    solidity = atof(doc.child("main").child("model").child("rotor").
           child_value("solidity"));
    AO = atof(doc.child("main").child("model").child("rotor").
           child_value("angleOfAttack"));
    twist = glo_pi / 180.0 * atof(doc.child("main").child("model").
            child("rotor").child_value("twist"));
    gamma = atof(doc.child("main").child("model").child("rotor").
           child_value("gamma"));
    xr = atof(doc.child("main").child("model").child("rotor").
           child_value("xr"));
    hr = atof(doc.child("main").child("model").child("rotor").
           child_value("hr"));
    delta0 = atof(doc.child("main").child("model").child("rotor").
           child_value("delta0"));
    delta2 = atof(doc.child("main").child("model").child("rotor").
           child_value("delta2"));

}

/////////////////////////////////////////////////////////

void c_Fuselage::init(){

    string s_input_folder = "../in_rtr/modelConfig.xml";
    const char* input_folder = s_input_folder.c_str();

    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(input_folder);

    cdsf = atof(doc.child("main").child("model").child("fuselage").
           child_value("cdsf"));

}

/////////////////////////////////////////////////////////

void c_Horizontal_Stabilizer::init(){

    string s_input_folder = "../in_rtr/modelConfig.xml";
    const char* input_folder = s_input_folder.c_str();

    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(input_folder);

    sht = atof(doc.child("main").child("model").child("horizontalStabilizer").
           child_value("sht"));
    aht = atof(doc.child("main").child("model").child("horizontalStabilizer").
           child_value("aht"));
    lht = atof(doc.child("main").child("model").child("horizontalStabilizer").
           child_value("lht"));

};

/////////////////////////////////////////////////////////

void c_Tail_Rotor::init(){

    string s_input_folder = "../in_rtr/modelConfig.xml";
    const char* input_folder = s_input_folder.c_str();

    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(input_folder);

    rtr = atof(doc.child("main").child("model").child("tailRotor").
           child_value("radius"));
    omegtr = 2.0 * glo_pi / 60.0 * (atof(doc.child("main").child("model").
           child("tailRotor").child_value("omega")));
    soltr = atof(doc.child("main").child("model").child("tailRotor").
           child_value("solidity"));
    aotr = atof(doc.child("main").child("model").child("tailRotor").
           child_value("angleOfAttack"));
    twisttr = glo_pi / 180.0 * atof(doc.child("main").child("model").
            child("tailRotor").child_value("twist"));
    ltr = atof(doc.child("main").child("model").child("tailRotor").
           child_value("ltr"));
    htr = atof(doc.child("main").child("model").child("tailRotor").
           child_value("htr"));

}

/////////////////////////////////////////////////////////


void c_Aircraft::init(){

    Rotor.init();
    Fuselage.init();
    Horizontal_Stabilizer.init();
    Tail_Rotor.init();

    string s_input_folder = "../in_rtr/modelConfig.xml";
    const char* input_folder = s_input_folder.c_str();

    pugi::xml_document doc;

    pugi::xml_parse_result result = doc.load_file(input_folder);

    _Air.density = atof(doc.child("main").child("environment").
                   child_value("density"));
    weight = atof(doc.child("main").child("model").child("aircraft").
           child_value("weight"));
    mass = weight / glo_g;
    Ixx = atof(doc.child("main").child("model").child("aircraft").
           child_value("Ixx"));
    Iyy = atof(doc.child("main").child("model").child("aircraft").
           child_value("Iyy"));
    Izz = atof(doc.child("main").child("model").child("aircraft").
           child_value("Izz"));
    Ixz = atof(doc.child("main").child("model").child("aircraft").
           child_value("Ixz"));

}

void c_Aircraft::calculateAerodynamicForcesAndMoments(double *_x, double *_u, double *_FM){

   static double lambda0_last;
   static double lambda0tr_last;

   double u = _x[0];
   double v = _x[1];
   double w = _x[2];
   double p = _x[3];
   double q = _x[4];
   double r = _x[5];

   double theta1c = _u[0] * glo_pi / 180.0;
   double theta1s = _u[1] * glo_pi / 180.0;
   double theta0  = _u[2] * glo_pi / 180.0;
   double theta0tr= _u[3] * glo_pi / 180.0;

   // Get hub velocities
   double uh = u - q * Rotor.hr;
   double vh = v + r * Rotor.xr + p * Rotor.hr;
   double wh = w - q * Rotor.xr;

   // Main rotor advance ratios
   double Vinplane = pow((pow(uh,2.0)+pow(vh,2.0)),0.5);
   double mu = Vinplane/(Rotor.omega*Rotor.radius);
   double muz = wh / (Rotor.omega*Rotor.radius); 
   double mu2 = mu*mu;

   // Get wind axis transformation
   double psiw = atan2(vh,uh);
   double cpsiw = cos(psiw);
   double spsiw = sin(psiw);
   double Th2w[2][2], Tw2h[2][2];

   Th2w[0][0] = cpsiw;
   Th2w[0][1] = spsiw;
   Th2w[1][0] = -spsiw;
   Th2w[1][1] = cpsiw;

   Tw2h[0][0] = Th2w[0][0];
   Tw2h[0][1] = Th2w[1][0];
   Tw2h[1][0] = Th2w[0][1];
   Tw2h[1][1] = Th2w[1][1];

//   // Non-dimensional angular rates in wind axes
   double phw = (Th2w[0][0]*p +  Th2w[0][1]*q) / Rotor.omega;
   double qhw = (Th2w[1][0]*p +  Th2w[1][1]*q) / Rotor.omega;
//  
//   // Cyclic pitch translated to wind axes
   double theta1sw = Th2w[0][0]*theta1s +  Th2w[0][1]*theta1c ;
   double theta1cw = Th2w[1][0]*theta1s +  Th2w[1][1]*theta1c ;

   // CT and lambda0 iteration, solves for inflow and thrust coefficient
   
   double lambda0 = 0.0;
//   double lambda0_last = 0.5;
   static int lambda_init = -1;
   if(lambda_init < 0){
       lambda0=0.05;
       lambda_init = 1;
   }
   else{
       lambda0=lambda0_last;
   }
   

   double delta_lambda = 1.0;
   int iter = 0;
   double CT, lamtot2;
   do{
     CT = 0.5 * Rotor.AO * Rotor.solidity * ((1.0/3.0+0.5*mu2) * theta0 + 0.5 * mu * (theta1sw + 0.5*phw) + 0.5 * (muz-lambda0)+ 0.25 * (1+mu2)*Rotor.twist);
     lamtot2 = mu2 + pow((lambda0-muz),2.0);
     delta_lambda = - 1.0 * (2.0 * lambda0 * pow(lamtot2,0.5)-CT) * lamtot2 / 
                    (2.0 * pow(lamtot2,1.5) + 0.25 * Rotor.AO * Rotor.solidity 
                    * lamtot2 - CT * (muz-lambda0));
     lambda0 += 0.5 * delta_lambda;
     iter++;
   }while((abs(delta_lambda)>1.0e-9) && (iter<200));
   lambda0_last = lambda0;

   // Thrust
   double T = CT * _Air.density * pow(Rotor.omega,2.0) * glo_pi * pow(Rotor.radius,4.0);

   // Uniform inflow assumption in this model
   double lambda1cw = 0.0;   
   double lambda1sw = 0.0;   

   // Quasi-Steady Flapping in Wind Axes (page 107)
   double Abt[4][2];
   double Abl[3][2];
   double Abo[2][2];

   Abt[0][0] = -8.0 / 3.0 * mu * (1.0 + 0.5 * mu2);
   Abt[1][0] = -2.0 * mu * (1.0 + 0.5 * mu2);
   Abt[2][0] = -(1.0+2.0*pow(mu, 2.0));
   Abt[3][0] = 0.0;
   Abt[0][1] = -Rotor.gamma / 6.0 * mu * (1.0 + 0.5 * mu2);
   Abt[1][1] = -2.0 * Rotor.gamma * mu / 15.0 * (1.0 + mu2 / 3.0);
   Abt[2][1] = -2.0 * Rotor.gamma * mu2 / 9.0;
   Abt[3][1] = 1.0 - 0.5 * mu2 * mu2;

   Abl[0][0] = -2.0 * mu * (1.0 + 0.5 * mu2);
   Abl[1][0] = 1.0 + 0.5 * pow(mu,2.0);
   Abl[2][0] = 0.0;
   Abl[0][1] = -2.0 * Rotor.gamma * mu / 9.0 * (1.0-0.5*mu2);
   Abl[1][1] = Rotor.gamma /9.0 *pow(mu,2.0) + 0.5 *Rotor.gamma/9.0 * mu2;
   Abl[2][1] = -(1.0-0.5*mu2);

   Abo[0][0] = -(1.0+0.5*pow(mu,2.0));
   Abo[1][0] = 16.0/Rotor.gamma * (1.0+0.5*mu2);
   Abo[0][1] = 16.0/Rotor.gamma * (1.0-0.5*mu2) + Rotor.gamma / 9.0 
   * pow(mu,2.0);
   Abo[1][1] = 1.0 - 0.5 * mu2;

   double temp[2];

   temp[0] = Abt[0][0] * theta0 + Abt[1][0] * Rotor.twist + Abt[2][0] * 
             theta1sw + Abt[3][0] * theta1cw +
             Abl[0][0] * (muz-lambda0) + Abl[1][0] * lambda1sw +
             Abl[2][0] * lambda1cw +
             Abo[0][0] * phw + Abo[1][0] * qhw;

   temp[1] = Abt[0][1] * theta0 + Abt[1][1] * Rotor.twist + Abt[2][1] * 
             theta1sw + Abt[3][1] * theta1cw +
             Abl[0][1] * (muz-lambda0) + Abl[1][1] * lambda1sw +
             Abl[2][1] * lambda1cw +
             Abo[0][1] * phw + Abo[1][1] * qhw;

   // Convert back to hub system
   double beta1sw = temp[0];
   double beta1cw = temp[1];
   temp[0] = Tw2h[0][0] * beta1sw + Tw2h[1][0] * beta1cw;
   temp[1] = Tw2h[0][1] * beta1sw + Tw2h[1][1] * beta1cw;

   double beta1s = temp[1];
   double beta1c = temp[0];

   // Torque
   double delta = Rotor.delta0 + Rotor.delta2 * CT * CT;
   double CQ = -1.0 * (muz-lambda0) * CT + 0.125 * delta * Rotor.solidity * 
               (1.0 + 7.0 / 3.0 * pow(mu,2.0));
   double Q = CQ * _Air.density * pow(Rotor.omega,2.0) * glo_pi * 
              pow(Rotor.radius,5.0);
  
   // Translate rotor forces and moments to CG
   double Xr = T * beta1c;
   double Yr = -T * beta1s;
   double Zr = -T;
   double Lr = Yr * Rotor.hr - 0.5 * Q * beta1c;
   double Mr = -Xr * Rotor.hr + T * Rotor.xr + 0.5 * Q * beta1s;
   double Nr = Yr * Rotor.xr + Q;

   // Fuselage
   // Calc total airspeed, alpha, beta
   double V = max(pow((pow(u,2.0)+pow(v,2.0)+pow(w,2.0)),0.5),1.0E-9);
   double alpha = atan2(w,max(u,1.0E-9));
   double beta = asin(v/V);

   // Drag Force
   double Df = 0.5 * _Air.density * pow(V,2.0) * Fuselage.cdsf;
   
   // Transform to body forces
   double Xf = -Df * cos(alpha) * cos(beta);
   double Yf = -Df * sin(beta);
   double Zf = -Df * sin(alpha) * cos(beta);

   // Horizontal Stabilizer

     // Calc. local alpha 
     double uht = u;
     double wht = w + q * Horizontal_Stabilizer.lht;
     double alphaht = atan2(wht,uht);
  
     // Lift Force
     double Clht = Horizontal_Stabilizer.aht * alphaht;
     double Lht = 0.5 * _Air.density * pow(V,2.0) * 
                  Horizontal_Stabilizer.sht * Clht;

     // Transform to body forces at CG
     double Zht = -Lht;
     double Mht = -Lht * Horizontal_Stabilizer.lht;

   // Tail Rotor
   
     // Local velocities at tail rotor hub in tail rotor CS
     double utr = u - q * Tail_Rotor.htr;
     double vtr = w + q * Tail_Rotor.ltr;
     double wtr = -v + r * Tail_Rotor.ltr - p * Tail_Rotor.htr;
     double mutr = pow((pow(utr,2.0)+pow(vtr,2.0)),0.5) / (Tail_Rotor.omegtr * Tail_Rotor.rtr);
     double muztr = wtr / (Tail_Rotor.omegtr * Tail_Rotor.rtr); 

   double lambda0tr = 0.0;
//   double lambda0_last = 0.5;
   static int lambdatr_init = -1;
   if(lambdatr_init < 0){
       lambda0tr=0.05;
       lambdatr_init = 1;
   }
   else{
       lambda0tr=lambda0tr_last;
   }
   
   double delta_lambdatr = 1.0;
   int itertr = 0;
   double CTtr, lamtot2tr;
   do{
     CTtr = 0.5 * Tail_Rotor.aotr * Tail_Rotor.soltr * ((1.0/3.0+0.5*pow(mutr,2.0)) * theta0tr + 0.5 * (muztr-lambda0tr) + 0.25 * (1.0 + pow(mutr,2.0))*Tail_Rotor.twisttr);
     lamtot2tr = pow(mutr,2.0) + pow((lambda0tr-muztr),2.0);
     delta_lambdatr = -(2.0 * lambda0tr *pow(lamtot2tr,0.5) - CTtr) * lamtot2tr
                      / (2.0 * pow(lamtot2tr,1.5) + 0.25 * Tail_Rotor.aotr 
                      * Tail_Rotor.soltr * lamtot2tr - CTtr * 
                      (muztr - lambda0tr));
     lambda0tr += 0.5 * delta_lambdatr;
     itertr++;
   }while((abs(delta_lambdatr)>1.0e-9) && (itertr<200));
   lambda0tr_last = lambda0tr;

   // Calc tail rotor thrust and transform to body frame at CG

   double Ttr = CTtr * _Air.density * pow(Tail_Rotor.omegtr,2.0) * 
                glo_pi * pow(Tail_Rotor.rtr,4.0); 
   double Ytr = Ttr;
   double Ltr = Ttr * Tail_Rotor.htr;
   double Ntr = -Ttr * Tail_Rotor.ltr;

//   cout << fixed;
//   cout.precision(10);
//   cout << "olmadik " << Ttr << " " << Ytr << " " << Ltr << " " << Ntr << '\n';

    //Sum total forces and moments
    
    X = Xr + Xf;
    Y = Yr + Yf + Ytr;
    Z = Zr + Zf + Zht;
    L = Lr + Ltr;
    M = Mr + Mht;
    N = Nr + Ntr;

    _FM[0] = X;
    _FM[1] = Y;
    _FM[2] = Z;
    _FM[3] = L;
    _FM[4] = M;
    _FM[5] = N;

}

/////////////////////////////////////////////////////////

void c_Trim::init(double Vx, double Vy, double Vz){

    Aircraft.init();

    _V_trim[0] = Vx;
    _V_trim[1] = Vy;
    _V_trim[2] = Vz;
    _psid_trim = 0.0;

    // Initial Trim estimates
    for(int i=0; i<12; i++){
      _x[i] = 0.0;
    }

    if(abs(_V_trim[0]) < 0.01){
      _V_trim[0] = 0.01;
    }

    _x[0] = _V_trim[0];
    _x[1] = _V_trim[1];
    _x[2] = _V_trim[2];

    _u[0] = 0.0;
    _u[1] = 0.0;
    _u[2] = 20.0;
    _u[3] = 25.0;
    
    nstates = 12;
    nfstates = 12;
    nctrls = 4;

    //xscale, delclin, delxlin
    
    xscale[0] = 1.0;   
    xscale[1] = 1.0;   
    xscale[2] = 1.0;   
    xscale[3] = 1.0 / 57.3;
    xscale[4] = 1.0 / 57.3;
    xscale[5] = 1.0 / 57.3;
    xscale[6] = 1.0 / 57.3;
    xscale[7] = 1.0 / 57.3;
    xscale[8] = 1.0 / 57.3;
    xscale[9] = 1.0;
    xscale[10] = 1.0;
    xscale[11] = 1.0;
 
    delclin[0] = glo_pi / 180.0 * 0.1;
    delclin[1] = glo_pi / 180.0 * 0.1;
    delclin[2] = glo_pi / 180.0 * 0.1;
    delclin[3] = glo_pi / 180.0 * 0.1;

    for(int i=0; i<12; i++){
      delxlin[i] = 0.1 * xscale[i];
    }

    // trim variables array
    trimVars[0] = 0;
    trimVars[1] = 1;
    trimVars[2] = 2;
    trimVars[3] = 3;
    trimVars[4] = 4;
    trimVars[5] = 5;
    trimVars[6] = 6;
    trimVars[7] = 7;
    trimVars[8] = 12;
    trimVars[9] = 13;
    trimVars[10] = 14;
    trimVars[11] = 15;

}

void c_Trim::linearize(double *x0, double *u0, double *x_dot,  Matrix &A_matrix, Matrix &B_matrix){

//    double x_dot[12];
    formEquationOfMotion(x0, u0, x_dot);

    double x_p[12], u_p[12];
    double x_dot_p1[12], x_dot_p2[12];

    for (octave_idx_type j = 0; j < 12; j++){

     for (octave_idx_type i = 0; i < 12; i++){
       x_p[i] = x0[i];
     }    
     x_p[j] = x_p[j] + delxlin[j];
     Aircraft.calculateAerodynamicForcesAndMoments(x_p, u0, _FM);
     formEquationOfMotion(x_p, u0, x_dot_p1);
     x_p[j] = x_p[j] - 2.0 * delxlin[j];
     Aircraft.calculateAerodynamicForcesAndMoments(x_p, u0, _FM);
     formEquationOfMotion(x_p, u0, x_dot_p2);
     for (octave_idx_type i = 0; i < 12; i++){
       A_matrix(i,j) = (x_dot_p1[i] - x_dot_p2[i]) / (2.0* delxlin[j]);
     }
     
    }

    for (octave_idx_type j = 0; j < 4; j++){

     for (octave_idx_type i = 0; i < 12; i++){
       u_p[i] = u0[i];
     }    
     u_p[j] = u_p[j] + delclin[j];
     Aircraft.calculateAerodynamicForcesAndMoments(x0, u_p, _FM);
     formEquationOfMotion(x0, u_p, x_dot_p1);
     u_p[j] = u_p[j] - 2.0 * delclin[j];
     Aircraft.calculateAerodynamicForcesAndMoments(x0, u_p, _FM);
     formEquationOfMotion(x0, u_p, x_dot_p2);
     for (octave_idx_type i = 0; i < 12; i++){
       B_matrix(i,j) = (x_dot_p1[i] - x_dot_p2[i]) / (2.0* delclin[j]);
     }
     
    }
  
}

void c_Trim::find(){

    double x_new[12];
    double u_new[4];

    for(int i=0; i<12; i++){
        x_new[i] = _x[i];
    }
    for(int j=0; j<4; j++){
        u_new[j] = _u[j];
    }

    Aircraft.calculateAerodynamicForcesAndMoments(_x, _u, _FM);
    formEquationOfMotion(_x, _u, _x_dot);

    int it = 0;
    double err = 100.0;
    double trim_tol = 5.0E-5;
    int itmax = 40;
    double x_dot_des[12], x_dot_err[12], x_dot0[12];

    x_dot_des[0] = 0.0;
    x_dot_des[1] = 0.0;
    x_dot_des[2] = 0.0;
    x_dot_des[3] = 0.0;
    x_dot_des[4] = 0.0;
    x_dot_des[5] = 0.0;
    x_dot_des[6] = 0.0;
    x_dot_des[7] = 0.0;
    x_dot_des[8] = _psid_trim;
    x_dot_des[9] = _V_trim[0];
    x_dot_des[10] = _V_trim[1];
    x_dot_des[11] = _V_trim[2];

    int ntrimvars = 12;
    Matrix A_matrix = Matrix (12, 12);
    Matrix B_matrix = Matrix (12, 4);
    Matrix preJac_matrix = Matrix (12, 16);
    Matrix Jac_matrix = Matrix (12, 12);
    Matrix inv_Jac_matrix = Matrix (12, 12);
    double Jacfinal_matrix[12][12];

    double kscale, kbest, trim_update[12];
    double err2[12], err_best;
    int itopt, isoln;

    double x0_best[12], u0_best[4];
    for (int i=0;i<12;i++){
      x0_best[i] = 0.0;
    }
    for (int i=0;i<4;i++){
      u0_best[i] = 0.0;
    }

    do{
      it++;
      Aircraft.calculateAerodynamicForcesAndMoments(x_new, u_new, _FM);
      formEquationOfMotion(x_new, u_new, x_dot0);
      for(int i=0; i<12; i++){
          x_dot_err[i] = x_dot0[i] - x_dot_des[i];
      }
      linearize(x_new, u_new, _x_dot, A_matrix, B_matrix);

////      cout << "off" << '\n';
      for (octave_idx_type i = 0; i < 12; i++){
       for (octave_idx_type j = 0; j < 12; j++){
         preJac_matrix(i,j) = A_matrix(i,j);
       }
      }
      for (octave_idx_type i = 0; i < 12; i++){
       for (octave_idx_type j = 12; j < 16; j++){
         preJac_matrix(i,j) = B_matrix(i,j-12);
       }
      }
      for (octave_idx_type i = 0; i < 12; i++){
       for (octave_idx_type j = 0; j < 8; j++){
         Jac_matrix(i,j) = preJac_matrix(i,j);
       }
      }
//      cout << Jac_matrix;
//      cout << "off" << '\n';
      for (octave_idx_type i = 0; i < 12; i++){
       for (octave_idx_type j = 12; j < 16; j++){
         Jac_matrix(i,j-4) = preJac_matrix(i,j);
       }
      }

      inv_Jac_matrix =  Jac_matrix.pseudo_inverse();

      for (octave_idx_type i = 0; i < 12; i++){
       for (octave_idx_type j = 0; j < 12; j++){
         Jacfinal_matrix[i][j] =  - inv_Jac_matrix(i,j);
       }
      }
 
      for (int i=0;i<12;i++){
          trim_update[i] = 0.0;
          for (int j=0;j<12;j++){
              trim_update[i] +=( Jacfinal_matrix[i][j]* x_dot_err[j]);
          }
      }

      kscale = 1.0;
      kbest = 0.0;
      err = 0.0;
      for (int i=0;i<12;i++){
          err2[i] = abs(x_dot_err[i])/xscale[i];
          if(err2[i] > err){
            err = err2[i];
          }
      }
      err_best = err;
      itopt = 0;
      double trimvec[16], trimvec_new[16];
      for (int i=0;i<12;i++){
         trimvec[i] = x_new[i]; 
         trimvec_new[i] = x_new[i]; 
      }
      for (int i=0;i<4;i++){
         trimvec[i+12] = u_new[i]; 
         trimvec_new[i+12] = u_new[i]; 
      }
      
      isoln = 0;
      double x0_temp[12], u0_temp[4];
      double x_dot[12];

      do{
        itopt++;
        for (int i=0;i<8;i++){
          trimvec_new[i]  = trimvec[i] + kscale * trim_update[i];
        }

        trimvec_new[8] = 0.0;
        trimvec_new[9] = 0.0;
        trimvec_new[10] = 0.0;
        trimvec_new[11] = 0.0;

        for (int i=12;i<16;i++){
          trimvec_new[i]  = trimvec[i] + kscale * trim_update[i-4];
        }
        for (int i=0;i<12;i++){
          x0_temp[i] = trimvec_new[i];
        }
        for (int i=12;i<16;i++){
          u0_temp[i-12] = trimvec_new[i];
        }
        Aircraft.calculateAerodynamicForcesAndMoments(x0_temp, u0_temp, _FM);
        formEquationOfMotion(x0_temp, u0_temp, x_dot);
        double dum_max = 0.0;
        for (int i=0;i<12;i++){
          x_dot_err[i] = x_dot[i] - x_dot_des[i];
          x_dot_err[i] = abs(x_dot_err[i]) / xscale[i];
          if(x_dot_err[i] > dum_max){
            dum_max = x_dot_err[i];
          }
        }
        err = dum_max;
//        cout << "ya cikarsa " << err << '\n';

        if(err<err_best){
          err_best = err;
          kbest = kscale;
          for (int i=0;i<12;i++){
            x0_best[i] = x0_temp[i];
          }
          for (int i=0;i<4;i++){
            u0_best[i] = u0_temp[i];
          }
          kscale = kscale - 0.05;
          isoln = 1;
        }
        else if(isoln == 1){
          itopt = 20;
        }
        else{
          kscale = kscale - 0.05;
        }
        
      }while(itopt<19);

      if(isoln == 1){
        for (int i=0;i<12;i++){
          x_new[i] = x0_best[i];
        }
        for (int i=0;i<4;i++){
          u_new[i] = u0_best[i];
        }
//        cout << "Trim Error " << err_best << '\n';
      }
      else{
        for (int i=0;i<12;i++){
          x_new[i] = x0_temp[i];
        }
        for (int i=0;i<4;i++){
          u_new[i] = u0_temp[i];
        }
//        cout << "Trim Error " << err << '\n';
      }
      
    }while((it<itmax) && (err>trim_tol));

    if(err>trim_tol){
      cout << "Trim not achieved." << '\n';
      itrim = 0; 
    }
    else{
      cout << "Trim achieved at Vx = " << _V_trim[0] << " ft/s "<< '\n';
      for(int i=0; i<12; i++){
//        cout << "x vector " << x_new[i] << '\n';
        _x[i] =  x_new[i];
      }
//      cout << "-------------" << '\n';
      for(int i=0; i<4; i++){
//        cout << "u vector " << u_new[i] << '\n';
        _u[i] =  u_new[i];
      }
      itrim = 1; 
    }

}

void c_Trim::outputPerformance(double &thrust, double &torque){

     Aircraft.calculatePerformance(_x, _u, thrust, torque);

}

void c_Aircraft::calculatePerformance(double *_x, double *_u, double &thrust,
                                      double &torque){

   static double lambda0_last;
   static double lambda0tr_last;

   double u = _x[0];
   double v = _x[1];
   double w = _x[2];
   double p = _x[3];
   double q = _x[4];
   double r = _x[5];

   double theta1c = _u[0] * glo_pi / 180.0;
   double theta1s = _u[1] * glo_pi / 180.0;
   double theta0  = _u[2] * glo_pi / 180.0;
   double theta0tr= _u[3] * glo_pi / 180.0;

   // Get hub velocities
   double uh = u - q * Rotor.hr;
   double vh = v + r * Rotor.xr + p * Rotor.hr;
   double wh = w - q * Rotor.xr;

   // Main rotor advance ratios
   double Vinplane = pow((pow(uh,2.0)+pow(vh,2.0)),0.5);
   double mu = Vinplane/(Rotor.omega*Rotor.radius);
   double muz = wh / (Rotor.omega*Rotor.radius); 
   double mu2 = mu*mu;

   // Get wind axis transformation
   double psiw = atan2(vh,uh);
   double cpsiw = cos(psiw);
   double spsiw = sin(psiw);
   double Th2w[2][2], Tw2h[2][2];

   Th2w[0][0] = cpsiw;
   Th2w[0][1] = spsiw;
   Th2w[1][0] = -spsiw;
   Th2w[1][1] = cpsiw;

   Tw2h[0][0] = Th2w[0][0];
   Tw2h[0][1] = Th2w[1][0];
   Tw2h[1][0] = Th2w[0][1];
   Tw2h[1][1] = Th2w[1][1];

//   // Non-dimensional angular rates in wind axes
   double phw = (Th2w[0][0]*p +  Th2w[0][1]*q) / Rotor.omega;
   double qhw = (Th2w[1][0]*p +  Th2w[1][1]*q) / Rotor.omega;
//  
//   // Cyclic pitch translated to wind axes
   double theta1sw = Th2w[0][0]*theta1s +  Th2w[0][1]*theta1c ;
   double theta1cw = Th2w[1][0]*theta1s +  Th2w[1][1]*theta1c ;

   // CT and lambda0 iteration, solves for inflow and thrust coefficient
   
   double lambda0 = 0.0;
//   double lambda0_last = 0.5;
   static int lambda_init = -1;
   if(lambda_init < 0){
       lambda0=0.05;
       lambda_init = 1;
   }
   else{
       lambda0=lambda0_last;
   }
   

   double delta_lambda = 1.0;
   int iter = 0;
   double CT, lamtot2;
   do{
     CT = 0.5 * Rotor.AO * Rotor.solidity * ((1.0/3.0+0.5*mu2) * theta0 + 0.5 * mu * (theta1sw + 0.5*phw) + 0.5 * (muz-lambda0)+ 0.25 * (1+mu2)*Rotor.twist);
     lamtot2 = mu2 + pow((lambda0-muz),2.0);
     delta_lambda = - 1.0 * (2.0 * lambda0 * pow(lamtot2,0.5)-CT) * lamtot2 / 
                    (2.0 * pow(lamtot2,1.5) + 0.25 * Rotor.AO * Rotor.solidity 
                    * lamtot2 - CT * (muz-lambda0));
     lambda0 += 0.5 * delta_lambda;
     iter++;
   }while((abs(delta_lambda)>1.0e-9) && (iter<200));
   lambda0_last = lambda0;

   // Thrust
   double T = CT * _Air.density * pow(Rotor.omega,2.0) * glo_pi * pow(Rotor.radius,4.0);

   // Uniform inflow assumption in this model
   double lambda1cw = 0.0;   
   double lambda1sw = 0.0;   

   // Quasi-Steady Flapping in Wind Axes (page 107)
   double Abt[4][2];
   double Abl[3][2];
   double Abo[2][2];

   Abt[0][0] = -8.0 / 3.0 * mu * (1.0 + 0.5 * mu2);
   Abt[1][0] = -2.0 * mu * (1.0 + 0.5 * mu2);
   Abt[2][0] = -(1.0+2.0*pow(mu, 2.0));
   Abt[3][0] = 0.0;
   Abt[0][1] = -Rotor.gamma / 6.0 * mu * (1.0 + 0.5 * mu2);
   Abt[1][1] = -2.0 * Rotor.gamma * mu / 15.0 * (1.0 + mu2 / 3.0);
   Abt[2][1] = -2.0 * Rotor.gamma * mu2 / 9.0;
   Abt[3][1] = 1.0 - 0.5 * mu2 * mu2;

   Abl[0][0] = -2.0 * mu * (1.0 + 0.5 * mu2);
   Abl[1][0] = 1.0 + 0.5 * pow(mu,2.0);
   Abl[2][0] = 0.0;
   Abl[0][1] = -2.0 * Rotor.gamma * mu / 9.0 * (1.0-0.5*mu2);
   Abl[1][1] = Rotor.gamma /9.0 *pow(mu,2.0) + 0.5 *Rotor.gamma/9.0 * mu2;
   Abl[2][1] = -(1.0-0.5*mu2);

   Abo[0][0] = -(1.0+0.5*pow(mu,2.0));
   Abo[1][0] = 16.0/Rotor.gamma * (1.0+0.5*mu2);
   Abo[0][1] = 16.0/Rotor.gamma * (1.0-0.5*mu2) + Rotor.gamma / 9.0 
   * pow(mu,2.0);
   Abo[1][1] = 1.0 - 0.5 * mu2;

   double temp[2];

   temp[0] = Abt[0][0] * theta0 + Abt[1][0] * Rotor.twist + Abt[2][0] * 
             theta1sw + Abt[3][0] * theta1cw +
             Abl[0][0] * (muz-lambda0) + Abl[1][0] * lambda1sw +
             Abl[2][0] * lambda1cw +
             Abo[0][0] * phw + Abo[1][0] * qhw;

   temp[1] = Abt[0][1] * theta0 + Abt[1][1] * Rotor.twist + Abt[2][1] * 
             theta1sw + Abt[3][1] * theta1cw +
             Abl[0][1] * (muz-lambda0) + Abl[1][1] * lambda1sw +
             Abl[2][1] * lambda1cw +
             Abo[0][1] * phw + Abo[1][1] * qhw;

   // Convert back to hub system
   double beta1sw = temp[0];
   double beta1cw = temp[1];
   temp[0] = Tw2h[0][0] * beta1sw + Tw2h[1][0] * beta1cw;
   temp[1] = Tw2h[0][1] * beta1sw + Tw2h[1][1] * beta1cw;

   double beta1s = temp[1];
   double beta1c = temp[0];

   // Torque
   double delta = Rotor.delta0 + Rotor.delta2 * CT * CT;
   double CQ = -1.0 * (muz-lambda0) * CT + 0.125 * delta * Rotor.solidity * 
               (1.0 + 7.0 / 3.0 * pow(mu,2.0));
   double Q = CQ * _Air.density * pow(Rotor.omega,2.0) * glo_pi * 
              pow(Rotor.radius,5.0);
  
   // Translate rotor forces and moments to CG
   double Xr = T * beta1c;
   double Yr = -T * beta1s;
   double Zr = -T;
   double Lr = Yr * Rotor.hr - 0.5 * Q * beta1c;
   double Mr = -Xr * Rotor.hr + T * Rotor.xr + 0.5 * Q * beta1s;
   double Nr = Yr * Rotor.xr + Q;

   // Fuselage
   // Calc total airspeed, alpha, beta
   double V = max(pow((pow(u,2.0)+pow(v,2.0)+pow(w,2.0)),0.5),1.0E-9);
   double alpha = atan2(w,max(u,1.0E-9));
   double beta = asin(v/V);

   // Drag Force
   double Df = 0.5 * _Air.density * pow(V,2.0) * Fuselage.cdsf;
   
   // Transform to body forces
   double Xf = -Df * cos(alpha) * cos(beta);
   double Yf = -Df * sin(beta);
   double Zf = -Df * sin(alpha) * cos(beta);

   // Horizontal Stabilizer

     // Calc. local alpha 
     double uht = u;
     double wht = w + q * Horizontal_Stabilizer.lht;
     double alphaht = atan2(wht,uht);
  
     // Lift Force
     double Clht = Horizontal_Stabilizer.aht * alphaht;
     double Lht = 0.5 * _Air.density * pow(V,2.0) * 
                  Horizontal_Stabilizer.sht * Clht;

     // Transform to body forces at CG
     double Zht = -Lht;
     double Mht = -Lht * Horizontal_Stabilizer.lht;

   // Tail Rotor
   
     // Local velocities at tail rotor hub in tail rotor CS
     double utr = u - q * Tail_Rotor.htr;
     double vtr = w + q * Tail_Rotor.ltr;
     double wtr = -v + r * Tail_Rotor.ltr - p * Tail_Rotor.htr;
     double mutr = pow((pow(utr,2.0)+pow(vtr,2.0)),0.5) / (Tail_Rotor.omegtr * Tail_Rotor.rtr);
     double muztr = wtr / (Tail_Rotor.omegtr * Tail_Rotor.rtr); 

   double lambda0tr = 0.0;
//   double lambda0_last = 0.5;
   static int lambdatr_init = -1;
   if(lambdatr_init < 0){
       lambda0tr=0.05;
       lambdatr_init = 1;
   }
   else{
       lambda0tr=lambda0tr_last;
   }
   
   double delta_lambdatr = 1.0;
   int itertr = 0;
   double CTtr, lamtot2tr;
   do{
     CTtr = 0.5 * Tail_Rotor.aotr * Tail_Rotor.soltr * ((1.0/3.0+0.5*pow(mutr,2.0)) * theta0tr + 0.5 * (muztr-lambda0tr) + 0.25 * (1.0 + pow(mutr,2.0))*Tail_Rotor.twisttr);
     lamtot2tr = pow(mutr,2.0) + pow((lambda0tr-muztr),2.0);
     delta_lambdatr = -(2.0 * lambda0tr *pow(lamtot2tr,0.5) - CTtr) * lamtot2tr
                      / (2.0 * pow(lamtot2tr,1.5) + 0.25 * Tail_Rotor.aotr 
                      * Tail_Rotor.soltr * lamtot2tr - CTtr * 
                      (muztr - lambda0tr));
     lambda0tr += 0.5 * delta_lambdatr;
     itertr++;
   }while((abs(delta_lambdatr)>1.0e-9) && (itertr<200));
   lambda0tr_last = lambda0tr;

   // Calc tail rotor thrust and transform to body frame at CG

   double Ttr = CTtr * _Air.density * pow(Tail_Rotor.omegtr,2.0) * 
                glo_pi * pow(Tail_Rotor.rtr,4.0); 
   double Ytr = Ttr;
   double Ltr = Ttr * Tail_Rotor.htr;
   double Ntr = -Ttr * Tail_Rotor.ltr;

//   cout << fixed;
//   cout.precision(10);
//   cout << "olmadik " << Ttr << " " << Ytr << " " << Ltr << " " << Ntr << '\n';

    //Sum total forces and moments
    
    X = Xr + Xf;
    Y = Yr + Yf + Ytr;
    Z = Zr + Zf + Zht;
    L = Lr + Ltr;
    M = Mr + Mht;
    N = Nr + Ntr;

    thrust = T;
    torque = Q;

}

void c_Trim::formEquationOfMotion(double *xi, double *ui, double *xi_dot){

    double u,v,w,p,q,r,phi,theta,psi;
    double X, Y, Z, L, M, N;

    u = xi[0];
    v = xi[1];
    w = xi[2];
    p = xi[3];
    q = xi[4];
    r = xi[5];
    phi = xi[6];
    theta = xi[7];
    psi = xi[8];

    X = _FM[0];
    Y = _FM[1];
    Z = _FM[2];
    L = _FM[3];
    M = _FM[4];
    N = _FM[5];

    double cphi = cos(phi);
    double sphi = sin(phi);
    double ctheta = cos(theta);
    double stheta = sin(theta);
    double cpsi = cos(psi);
    double spsi = sin(psi);

    cout << fixed;
    cout.precision(10);

    xi_dot[0] = X / Aircraft.mass - glo_g * stheta        - q*w + r*v;
    xi_dot[1] = Y / Aircraft.mass + glo_g * ctheta * sphi - r*u + p*w;
    xi_dot[2] = Z / Aircraft.mass + glo_g * ctheta * cphi - p*v + q*u;

    double Ixx = Aircraft.Ixx;
    double Iyy = Aircraft.Iyy;
    double Izz = Aircraft.Izz;
    double Ixz = Aircraft.Ixz;

    double gam = Ixx * Izz - pow(Ixz, 2.0);
    xi_dot[3] = (Izz * L + Ixz * N + Ixz * (Ixx- Iyy + Izz) * p * q -
          (pow(Ixx,2.0)- Ixx * Iyy + pow(Ixz, 2.0)) * p * q ) / gam;
    xi_dot[4] = (M + (Izz-Ixx) * p * r - Ixz * (pow(p,2.0)-pow(r,2.0))) / Iyy;
    xi_dot[5] = (Ixx * N + Ixz * L - Ixz * (Ixx - Iyy + Izz) * q * r + 
          (pow(Ixx,2.0)- Ixx * Iyy + pow(Ixz,2.0)) * p * q) / gam;

    xi_dot[6] = p  + q * sphi * stheta / ctheta + r * cphi * stheta / ctheta;
    xi_dot[7] = q * cphi - r * sphi;
    xi_dot[8] = q * sphi / ctheta + r * cphi / ctheta;

    xi_dot[9] = u * ctheta * cpsi + v * (sphi * stheta * cpsi - cphi * spsi) +
               w * (cphi * stheta * cpsi + sphi * spsi);
    xi_dot[10]= u * ctheta * spsi + v * (sphi * stheta * spsi + cphi * cpsi) +
               w * (cphi * stheta * spsi - sphi * cpsi);
    xi_dot[11]= -u * stheta + v * sphi *ctheta + w * cphi * ctheta;

}

