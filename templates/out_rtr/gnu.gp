# First create a folder named 'plots' so that plots can be put into.
set term png medium
set style line 1 linetype 1 linewidth 1.0 pt 7 ps 1.05
set style line 2 linetype 2 linewidth 1.0 pt 7 ps 1.05
set style line 3 linetype 3 linewidth 1.0 pt 7 ps 1.05
set style line 4 linetype 4 linewidth 1.0 pt 7 ps 1.05
set style line 5 linetype 5 linewidth 1.0 pt 7 ps 1.05
set style line 6 linetype 6 linewidth 1.0 pt 7 ps 1.05

set grid ytics lw 1 lt 0
set grid xtics lw 1 lt 0

set key right top
set output "rtrbt_thrust.png"
set xlabel 'Velocity (ft/s)'
set ylabel 'Thrust'
p 'perf.dat' u 1:2 ls 1 w lp t 'Thrust'

set key right top
set output "rtrbt_torque.png"
set xlabel 'Velocity (ft/s)'
set ylabel 'Torque'
p 'perf.dat' u 1:3 ls 1 w lp t 'Torque'
